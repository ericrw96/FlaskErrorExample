from flask import Flask, jsonify, send_file

from ExceptionWithJson import ExceptionWithJson

app = Flask(__name__)


@app.route('/hello_world')
def hello_world():
    raise ExceptionWithJson("Forbidden", 403, {'test': True})


@app.route('/')
def serve_index():
    return send_file('templates/index.html')



@app.errorhandler(ExceptionWithJson)
def handle_exception_with_json(error):
    response = jsonify(error.to_dict())
    print(error.to_dict())
    response.status_code = error.status_code
    return response


if __name__ == '__main__':
    app.run()
