class ExceptionWithJson(Exception):
    def __init__(self, message, status_code, payload=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        exception_dict = dict(self.payload or ())
        exception_dict['message'] = self.message
        exception_dict['status_code'] = self.status_code
        return exception_dict



